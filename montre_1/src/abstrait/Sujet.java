package abstrait;

import java.util.ArrayList;

public class Sujet {

    public ArrayList<Observeur> lesObserveurs = new ArrayList<Observeur>();

    public void Attache(Observeur o) {
        lesObserveurs.add(o);
    }

    public void Detache(Observeur o) {
        lesObserveurs.remove(o);
    }

    public void DetacheTous() {
        lesObserveurs.clear();
    }

    public void Notifie() {
        for (Observeur o : lesObserveurs) {
            o.MiseAJour();
        }
    }
}
