import java.util.Date;

public class Timer extends Thread {

    public Montre montre;

    public Timer() {
        super("Timer");
        montre = new Montre();
        montre.addPropertyChangeListener(new MontreDigitaleAnglaise());
        montre.addPropertyChangeListener(new MontreDigitaleFrancaise());
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                montre.setDate(new Date());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
