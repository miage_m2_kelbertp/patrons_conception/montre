import java.beans.PropertyChangeEvent;
import java.text.SimpleDateFormat;

public class MontreDigitaleAnglaise implements java.beans.PropertyChangeListener {
    public void propertyChange(PropertyChangeEvent evt) {
        SimpleDateFormat date = new SimpleDateFormat("MMM d, yyyy, h:mm:ss a");
        System.out.println(date.format(evt.getNewValue()));
    }
}
