import java.beans.PropertyChangeEvent;
import java.text.SimpleDateFormat;

public class MontreDigitaleFrancaise implements java.beans.PropertyChangeListener {
    public void propertyChange(PropertyChangeEvent evt) {
        SimpleDateFormat date = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        System.out.println(date.format(evt.getNewValue()));
    }

}
