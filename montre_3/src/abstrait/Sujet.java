package abstrait;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Observer;

public class Sujet {

    public ArrayList<Observer> lesObserveurs = new ArrayList<Observer>();

    public void Attache(Observer o) {
        lesObserveurs.add(o);
    }

    public void Detache(Observer o) {
        lesObserveurs.remove(o);
    }

    public void DetacheTous() {
        lesObserveurs.clear();
    }

    public void Notifie() {
        ListIterator<Observer> it = lesObserveurs.listIterator();
        while (it.hasNext()) {
            it.next().update(null, null);
        }
    }
}
