import java.text.SimpleDateFormat;
import java.util.Observable;
import java.util.Observer;

public class MontreDigitaleFrancaise implements Observer {
    public Montre montre;

    public MontreDigitaleFrancaise(Montre montre) {
        this.montre = montre;
        montre.Attache(this);
    }

    public void update() {
        SimpleDateFormat date = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        System.out.println(date.format(montre.getDate()));
    }

    @Override
    public void update(Observable o, Object arg) {
        SimpleDateFormat date = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        System.out.println(date.format(montre.getDate()));
    }
}
