public class Main {
    public static void main(String[] args) {
        Timer timer = new Timer();
        new MontreDigitaleAnglaise(timer.montre);
        new MontreDigitaleFrancaise(timer.montre);
        timer.start();
    }
}
