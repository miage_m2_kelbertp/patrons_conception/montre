import java.text.SimpleDateFormat;

public class MontreDigitaleAnglaise implements Observeur {
    public Montre montre;

    public MontreDigitaleAnglaise(Montre montre) {
        this.montre = montre;
        montre.Attache(this);
    }

    @Override
    public void MiseAJour() {
        SimpleDateFormat date = new SimpleDateFormat("MMM d, yyyy, h:mm:ss a");
        System.out.println(date.format(montre.getDate()));
    }
}
