package abstrait;

import java.util.ArrayList;
import java.util.ListIterator;

public class Sujet {

    public ArrayList<Observeur> lesObserveurs = new ArrayList<Observeur>();

    public void Attache(Observeur o) {
        lesObserveurs.add(o);
    }

    public void Detache(Observeur o) {
        lesObserveurs.remove(o);
    }

    public void DetacheTous() {
        lesObserveurs.clear();
    }

    public void Notifie() {
        ListIterator<Observeur> it = lesObserveurs.listIterator();
        while (it.hasNext()) {
            it.next().MiseAJour();
        }
    }
}
