import abstrait.Sujet;

import java.util.Date;

public class Montre extends Sujet {
    public Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        Notifie();
    }
}
