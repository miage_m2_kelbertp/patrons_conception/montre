import java.text.SimpleDateFormat;

public class MontreDigitaleFrancaise implements Observeur {
    public Montre montre;

    public MontreDigitaleFrancaise(Montre montre) {
        this.montre = montre;
        montre.Attache(this);
    }

    @Override
    public void MiseAJour() {
        SimpleDateFormat date = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        System.out.println(date.format(montre.getDate()));
    }
}
