import java.util.Date;
import java.util.Observable;

public class Montre extends Observable {
    public Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        setChanged();
        notifyObservers();
    }
}
