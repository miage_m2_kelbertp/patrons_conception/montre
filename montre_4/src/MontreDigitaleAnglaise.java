import java.text.SimpleDateFormat;
import java.util.Observable;
import java.util.Observer;

public class MontreDigitaleAnglaise implements Observer {
    public Montre montre;

    @Override
    public void update(Observable o, Object arg) {
        SimpleDateFormat date = new SimpleDateFormat("MMM d, yyyy, h:mm:ss a");
        System.out.println(date.format(montre.getDate()));
    }
}
